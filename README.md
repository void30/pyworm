##INSTRUCTIONS

1- This project requires some modules to be installed on both sides (server and client):

* Server side:

> pip install MySQL-python paramiko pycrypto json

* Client Machine 

> pip install psutil pycrypto pywin32api json

2- Edit the following in **constants.py**:

* DB_HOSTNAME change this variable to your database hostname.
* DB_USERNAME change it to your database's username.
* DB_PASSWORD change it to your database's password.
* DB_DATABASE change it to your database's name.
* DB_TABLE_NAME change it to your database's table name.
* SOCKET_IP change it to the same ip where 'server.py' will run.
* PORT change it to any port number.
* SMTP_SERVER change it to smtp server.
* FROM_ADDR chage it to the mail sender address.

3- Edit **config.xml** to add the clients data using the following format:
    
> <client ip="" port="" username="" password="password" mail="">
        <alert type="memory" limit="xx%" />
        <alert type="cpu" limit="xx%" />
    </client>

5- Run **server.py**.