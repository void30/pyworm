class User:

    def __init__(self, username="N/A", ip="N/A",
                 passwd="N/A", port="N/A",
                 mail="N/A", mem_limit="N/A",
                 cpu_limit="N/A"):
        self.username = username
        self.ip = ip
        self.passwd = passwd
        self.port = port
        self.mail = mail
        self.mem_limit = mem_limit
        self.cpu_limit = cpu_limit

    def __str__(self):
        return self.username
