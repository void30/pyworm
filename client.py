import base64
import json
import platform
import socket
import psutil
import time

from Crypto.Cipher import AES
from constants import *

cur_os = platform.system().lower()

if cur_os == 'windows':
    import win32evtlog

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

data = {}


def get_event_log():
    hand = win32evtlog.OpenEventLog('localhost', 'Security')
    flags = win32evtlog.EVENTLOG_BACKWARDS_READ | win32evtlog.EVENTLOG_SEQUENTIAL_READ

    events = win32evtlog.ReadEventLog(hand, flags, 0)

    data['logs'] = {}

    data['logs']['event_category'] = str(events[0].EventCategory)
    data['logs']['time_generated'] = str(events[0].TimeGenerated)
    data['logs']['source_name'] = str(events[0].SourceName)
    data['logs']['event_id'] = str(events[0].EventID)
    data['logs']['event_type'] = str(events[0].EventType)
    data['logs']['data'] = str(events[0].StringInserts)


cpu_prcnt = psutil.cpu_percent()
ram_prcnt = psutil.virtual_memory()[2]

s = int(time.time()) - int(psutil.boot_time())

# Converting the boot time to "DD:HH:MM:SS" format
minute = 60
hour = minute * 60
day = hour * 24

d = h = m = 0

d = s / day
s -= d * day
h = s / hour
s -= h * hour
m = s / minute
s -= m * minute

uptime = "%02d:%02d:%02d:%02d" % (d, h, m, s)

sock.connect((SOCKET_IP, PORT))

data['cpu'] = cpu_prcnt
data['ram'] = ram_prcnt
data['uptime'] = uptime

if cur_os == "windows":
    get_event_log()

encryption_obj = AES.new('This is a key123', AES.MODE_CFB, 'This is an IV456')
cipher_text = base64.encodestring(encryption_obj.encrypt(json.dumps(data)))

sock.sendall(cipher_text)
