# Database constants
DB_HOSTNAME = ''
DB_USERNAME = ''
DB_PASSWORD = ''
DB_DATABASE = ''
DB_TABLE_NAME = ''

# Database Strings
CREATE_DATABASE = 'CREATE DATABASE IF NOT EXISTS %s' % DB_DATABASE
CREATE_TABLE = """CREATE TABLE IF NOT EXISTS %s (
    id INT NOT NULL AUTO_INCREMENT, 
    ip VARCHAR(12) NOT NULL, 
    port INT NOT NULL, 
    username VARCHAR(50) NOT NULL,
    passwd VARCHAR(50) NOT NULL,
    mail VARCHAR(100) NOT NULL, 
    mem_limit FLOAT NOT NULL,
    cpu_limit FLOAT NOT NULL,
    mem_prcnt FLOAT NOT NULL,
    cpu_prcnt FLOAT NOT NULL,
    uptime VARCHAR(50) NOT NULL,
    PRIMARY KEY (id))""" % DB_TABLE_NAME

INSERT_INTO_TABLE = """INSERT INTO %s(ip, port, username, passwd, mail, 
        mem_limit, cpu_limit, mem_prcnt, cpu_prcnt, uptime)
        VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s', 
        '%s', '%s', '%s')"""

# Socket Variables
SOCKET_IP = ''
PORT = 5005
BUFFER_SIZE = 1024

# SMTP Variables
SMTP_SERVER = 'localhost'
FROM_ADDR = 'aaa@aaa.com'