import base64
import smtplib
import xml.etree.ElementTree as parser
import socket
import json
import os
import MySQLdb
import constants

from Crypto.Cipher import AES
from paramiko.ssh_exception import NoValidConnectionsError, AuthenticationException
from ssh import *
from User import User

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)


def setup_database():
    db_conn = MySQLdb.connect(constants.DB_HOSTNAME,
                              constants.DB_USERNAME,
                              constants.DB_PASSWORD)
    db_conn.autocommit(on=True)
    cur = db_conn.cursor()
    cur.execute(constants.CREATE_DATABASE)
    cur.execute("USE %s" % constants.DB_DATABASE)
    cur.execute(constants.CREATE_TABLE)

    return db_conn


# Parsing the 'config.xml file and fetching the results into a variable
def fetch_config():
    tree = parser.parse('config.xml')
    root = tree.getroot()

    users = []

    for data in root.findall('client'):
        ip = data.get('ip')
        port = data.get('port')
        passwd = data.get('password')
        username = data.get('username')
        email = data.get('mail')

        for alert in data.findall('alert'):
            if alert.get('type') == 'memory':
                mem_limit = alert.get('limit').replace('%', '')
            elif alert.get('type') == "cpu":
                cpu_limit = alert.get('limit').replace('%', '')

        users.append(User(ip=ip,
                          port=port,
                          passwd=passwd,
                          username=username,
                          mail=email,
                          mem_limit=mem_limit,
                          cpu_limit=cpu_limit))
    return users


def send_mail(from_addr, to_addr, body):

    server = smtplib.SMTP(constants.SMTP_SERVER)
    server.set_debuglevel(1)
    server.sendmail(from_addr, to_addr, body)
    server.quit()


users = fetch_config()
db_conn = setup_database()

for user in users:
    data = None

    try:
        conn = ssh(user.ip, user.username, user.passwd)

        conn.send_command("mkdir temp")
        path_to_file = os.path.join("temp", "client.py")
        conn.send_file("client.py", path_to_file)

        sock.bind((constants.SOCKET_IP, constants.PORT))
        sock.listen(1)

        conn.send_command("python %s" % path_to_file)

        socket_conn, addr = sock.accept()

        while 1:
            encrypted_data = socket_conn.recv(constants.BUFFER_SIZE)
            decryption_obj = AES.new('This is a key123', AES.MODE_CFB, 'This is an IV456')
            data = json.loads(decryption_obj.decrypt(base64.decodestring(encrypted_data)))

            if data:
                break

        socket_conn.close()

    except NoValidConnectionsError:
        print "Could not connect to '%s'" % user.ip
    except AuthenticationException:
        print "Authentication Error"

    if data:
        if int(data['ram']) >= int(user.mem_limit):
            body = "Memory percentage is: %s%%" % data['ram']
            try:
                send_mail(constants.FROM_ADDR, user.mail, body)
            except socket.error:
                print "could not send mail"

        if int(data['cpu']) >= int(user.cpu_limit):
            body = "CPU Usage percentage is: %s%%" % data['cpu']

            try:
                send_mail(constants.FROM_ADDR, user.mail, body)
            except socket.error:
                print "could not send mail"

        cur = db_conn.cursor()

        sql = constants.INSERT_INTO_TABLE % (constants.DB_TABLE_NAME,
                                             user.ip, user.port,
                                             user.username, user.passwd,
                                             user.mail, user.mem_limit,
                                             user.cpu_limit, data['ram'],
                                             data['cpu'], data['uptime'])

        cur.execute(sql)

        if 'logs' in data.keys():
            body = """Event Source Name: %s
            Event Type: %s
            Event ID: %s
            Event Category: %s
            Event Data: %s""" % (data['logs']['source_name'],
                                 data['logs']['event_type'],
                                 data['logs']['event_id'],
                                 data['logs']['event_category'],
                                 data['logs']['data'])
            try:
                send_mail(constants.FROM_ADDR, 'to_addr', body)
            except socket.error:
                print "could not send mail"

db_conn.close()
