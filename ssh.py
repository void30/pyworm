from paramiko import client


class ssh:
    client = None

    def __init__(self, address, username, password):
        print("Connecting to '%s'" % address)

        self.client = client.SSHClient()
        self.client.set_missing_host_key_policy(client.AutoAddPolicy())
        self.client.connect(address, username=username, password=password, look_for_keys=False)

    def send_command(self, command):
        if self.client:
            stdin, stdout, stderr = self.client.exec_command(command)

            while not stdout.channel.exit_status_ready():
                if stdout.channel.recv_ready():
                    alldata = stdout.channel.recv(1024)

                    while stdout.channel.recv_ready():
                        alldata += stdout.channel.recv(1024)

                    print alldata
        else:
            print("Connection not opened.")

    def send_file(self, source, dest):
        sftp = self.client.open_sftp()
        sftp.put(source, dest)
