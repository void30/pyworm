import unittest
import User

class testUser(unittest.TestCase):

    def test_user(self):
        user = User.User('aa', '127.0.0.1', "12345", "15", "aaa@aaa", "20", "20")

        self.assertEqual(user.username, 'aa')
        self.assertEqual(user.ip, '127.0.0.1')
        self.assertEqual(user.passwd, '12345')
        self.assertEqual(user.port, '15')
        self.assertEqual(user.mail, 'aaa@aaa')
        self.assertEqual(user.mem_limit, '20')
        self.assertEqual(user.cpu_limit, '20')

    def test_user2(self):
        user = User.User()
        self.assertEqual(user.username, 'N/A')
        self.assertEqual(user.ip, 'N/A')
        self.assertEqual(user.passwd, 'N/A')
        self.assertEqual(user.port, 'N/A')
        self.assertEqual(user.mail, 'N/A')
        self.assertEqual(user.mem_limit, 'N/A')
        self.assertEqual(user.cpu_limit, 'N/A')

    def test_str(self):
        user1 = User.User(username="aa")

        self.assertEqual(str(user1), 'aa')


if __name__ == '__main__':
    unittest.main()